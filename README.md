# Запуск 
1. Переходим в директорию проекта и выполняем команды:
```
docker image build -t go_app:1.0 .
docker container run --rm -dit --name my-go-app -p 80:3000 go_app:1.0
```
2. Проверяем работу командой
```
curl http://localhost/
```