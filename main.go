package main

import (
    "html/template"
    "net/http"
    "os"
)




func indexHandler(w http.ResponseWriter, r *http.Request) {
    tpl := template.Must(template.ParseFiles("templates/index.html"))
    if r.URL.Path != "/" {
        http.NotFound(w,r)
        return
    }
    tpl.Execute(w, nil)
}

func main() {
    port := os.Getenv("PORT")
    if port == "" {
        port = "3000"
    }

    fs := http.FileServer(http.Dir("assets"))

    mux := http.NewServeMux()
    mux.Handle("/assets/", http.StripPrefix("/assets/", fs))
    mux.HandleFunc("/", indexHandler)
    http.ListenAndServe(":"+port, mux)
}
