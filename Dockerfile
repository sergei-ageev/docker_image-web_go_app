FROM golang:1.20.2-alpine3.17 AS builder

RUN apk update && apk add --no-cache git

ENV USER=appuser
ENV UID=10001
ENV APP=go_app

RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "/nonexistent" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid "${UID}" \
    "${USER}"

WORKDIR $GOPATH/src/mypackage/$APP

COPY go.mod main.go .

RUN go mod download && \
    go mod verify && \
    GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o "/go/bin/${APP}"

FROM scratch

LABEL mainteiner="Sergei Ageev"

ENV APP=go_app

WORKDIR $APP

COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group
COPY --from=builder /go/bin/$APP /$APP/$APP
COPY templates ./templates
COPY assets ./assets

USER appuser:appuser

ENTRYPOINT ["/go_app/go_app"]
